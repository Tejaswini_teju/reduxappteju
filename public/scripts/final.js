const { createClass, PropTypes } = React;
const Display = createClass({
    contextTypes: {
        store: PropTypes.object
    },
    componentDidMount() {
        const { store } = this.context;
        this.unsubscribe = store.subscribe( () => this.forceUpdate() )
    },
    componentWillUnmount() {
        this.unsubscribe(); 
    },
    render() {

    	const { store } = this.context;
		const { val } = this.context.store.getState();

        return(
		<div className="Display">
		<h1>Hi my name </h1>
		<form>
		    <input type='text'
		           name='a_value'
		           placeholder='Enter 1st num'
		           onChange={ (e) => dispatch( change_a(e.target.value) ) }
		    />
		    <input type='text'
		           name='b_value'
		           placeholder='Enter 2nd num'
		           onChange={ (e) => dispatch( change_b(e.target.value) ) }
		    />
		     <button onClick={() => dispatch(op_update('ADD'))}>+</button>  
				<button onClick={() => dispatch(op_update('SUB'))}>-</button>  
				<button onClick={() => dispatch(op_update('MUL'))}>x</button>  
				<button onClick={() => dispatch(op_update('DIV'))}>/</button>
						
		   <button onClick={() => {store.dispatch(perform_action(val))}}> = </button> 
		    
		    
		</form>
		
		<p>Answer is: {val.answer}</p>
		</div>  
		);
		}
});

let nextId = 0


const change_a = (a=0) => {
  return {
    type: 'CHANGE_A',
    a
  }
}

const change_b = (b=0) => {
  return {
    type: 'CHANGE_B',
    b
  }
}

const perform_action = (values={answer:0,a:0,b:0,op:''}) => {
  return {
    type: 'PERFORM_ACTION',
    id: nextId++,
    a:values.a,
    b:values.b,
    op:values.op
  }
  
  
}

const op_update = (op = '') => {
return {
	type: 'OPUPDATE',
	op
	}

}




const get_answer = (a,b,op) => {
  switch(op){
  	case 'ADD': return (a+b)
  	case 'SUB': return (a-b)
  	case 'MUL': return (a*b)
  	case 'DIV': return (a/b)
  	default   : return a
  }
	
}

const calculator = (state = {answer:0,a:0,b:0,op:''}, action) => {
  switch (action.type) {
    
    case 'OPUPDATE':
      return {
      			...state,
      			op:action.op
      		 }
    case 'CHANGE_A':
      return { 
      			...state,
      			a:action.a
  			 }
  			 
  	case 'CHANGE_B':
      return { 
      			...state,
      			b:action.b
  			 }
    case 'PERFORM_ACTION':
      return {...state,
      			a:action.a,
      			b:action.b,
      			answer: get_answer(action.a,action.b,action.op)
      			
      		}
    default:
      return state
  }
};

const { createStore } = Redux;
const store = createStore(calculator);
const { getState, dispatch } = store;
console.log(getState());


const { Provider } = ReactRedux;
ReactDOM.render(
    <Provider store={ store }>
        <Display />
    </Provider>,
    document.querySelector('#content')
);
